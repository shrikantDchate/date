#ifndef DATE_H_
#define DATE_H_
class date
{
private:
	int day,month,year;
public:
	date();
	date(int day,int month,int year);
	void accept();
	void display();
	void set_day(int day);
	int get_day();
	void set_month(int month);
	int get_month();
	void set_year(int year);
	int get_year();
	virtual ~date();
};


#endif /* DATE_H_ */
